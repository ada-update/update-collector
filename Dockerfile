FROM gradle:7-jdk11 AS build

COPY / .

RUN gradle --no-daemon clean assemble


FROM openjdk:11-jre-slim

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY --from=build "/home/gradle/build/libs/update-collector-1.0.0-SNAPSHOT-standalone.jar" app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
