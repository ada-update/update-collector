# ADA Update | Update Collector (Java)

The Update Collector is a piece of software that collects all information (package manager locations, dependencies,
updates, pending request) and creates Merge Requests/Pull requests depending on the environment.

# Ada Update | Manager Portal 

The manager [Ada Manager](https://mgr.adaupdate.io) manages all your project dependancies. You can manage your projects, 
update settings, view pending updates, enable/disable package managers and more.

## How does it work

The Update Collector runs in a container in your CI environment, therefore **we never have access to your source code**.
When you create a project in the manager you receive a secret which you will have to store in your CI provider (f.e. 
GitLab/GitHub). The Update Collector sends this secret to the manager so the rest of the data can be matched to the
corresponding project.

### Package managers

The Update Collector looks for specific files in the project directory (like composer/yarn lock files) to deduce what
package managers have been used. This information - the type, and relative directory - is sent to the manager, in which
you can select which ones to manage and which to ignore. Based on that information the collector will continue it's work
in looking for dependencies.

### Dependencies

As we already know where the package managers are, the next step is to do a clean install (in the update container) and to 
collect which dependencies have been installed and which versions they are running on. This information is then sent back 
to the manager which will return a list of dependencies to update, based on version information (if there is a major, minor 
or patch update), pending updates. By default, a maximum of 5 updates will be created. If there are pending updates it will
subtract this number.

### Automated updates

For each dependency the manager tells us to update, a new branch is created, with a fresh install and the specific
package is updated. Then all changes are committed and a merge request is created. If one already exists the current is
updated if need be, f.e. if a newer version has been released since the last time.
