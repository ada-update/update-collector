package io.ada.update.managers

import io.ada.update.data.Service
import io.ada.update.execute
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ComposerPackageManagerTest {

    private val composerDir = "./src/test/resources/composer"
    private val service = Service(-1, "test", composerDir, composerDir)

    @BeforeAll
    fun initComposer() {
        "composer i".execute(File(composerDir))
    }

    @Test
    fun testCollect() {

        val data = ComposerPackageManager().collectDependencies(service)

        assertEquals(1, data.size)
        assertEquals(data[0].identifier, "phpunit/phpunit")
        assertEquals(data[0].current_version, "9.4.0")
        assertNotEquals(data[0].current_version, data[0].newest_version)
    }
}
