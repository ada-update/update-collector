package io.ada.update

import io.ktor.http.*
import io.ktor.http.URLProtocol.Companion.HTTP
import io.ktor.http.URLProtocol.Companion.HTTPS
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.assertEquals
import org.junit.jupiter.params.provider.Arguments.of as args

class UtilsTest {

    @ParameterizedTest
    @MethodSource("urlValues")
    fun test(input: String, protocol: URLProtocol, host: String, port: Int) {

        val builder = URLBuilder()
        buildUrl(input, builder)

        assertEquals(protocol, builder.protocol)
        assertEquals(host, builder.host)
        assertEquals(port, builder.port)
    }

    companion object {

        @JvmStatic
        fun urlValues(): Stream<Arguments> = Stream.of(
            args("gitlab.com", HTTP, "gitlab.com", 80),
            args("gitlab.com/test/lalala", HTTP, "gitlab.com", 80),
            args("gitlab.com:8000", HTTP, "gitlab.com", 8000),
            args("gitlab.com:9000/test", HTTP, "gitlab.com", 9000),
            args("http://google.com:3000/tset", HTTP, "google.com", 3000),
            args("https://www.gitloab.com:3454/test", HTTPS, "www.gitloab.com", 3454),
            args("ftp://test.nl:33/test", HTTP, "test.nl", 33),
            args("vnc://178.54.19.6", HTTP, "178.54.19.6", 80),
            args("vnc://257", HTTP, "257", 80),
            args("https://example.com", HTTPS, "example.com", 443),
        )
    }
}
