package io.ada.update

import io.ada.update.actions.gitlab.GitLabActionExecutor
import io.ada.update.data.Check
import io.ada.update.data.Dependency
import io.ada.update.data.ManagerData
import io.ada.update.data.Service
import io.ada.update.managers.ComposerPackageManager
import io.ada.update.managers.PackageManager
import io.ada.update.portal.PortalClient
import io.ada.update.portal.requests.ActionResponse
import io.ada.update.portal.responses.Action
import java.io.File
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set

private typealias ManagerStatus = Pair<List<Check>, List<Dependency>>

val packageManagerRegistry: Map<String, PackageManager> = listOf(
    ComposerPackageManager(),
).fold(mutableMapOf()) { acc, manager -> acc[manager.name] = manager; acc }

class UpdateCollector {

    private val client = PortalClient()

    // TODO discover environment (gitlab/github/etc)

    suspend fun discoverServices(dir: String): List<Service> {
        println("Searching for package managers in $dir")
        val managers = packageManagerRegistry
            .map { (type, manager) -> manager.discoverManagers(dir).map { ManagerData(type, it) } }
            .flatten()

        managers.forEach { println("Found ${it.type} in ${it.path}") }
        println("Sending data to manager")

        return client
            .patchManagers(managers)
            .map { it.toService(dir) }
    }

    suspend fun collect(services: List<Service>): MutableMap<Int, ManagerStatus> {
        // TODO execute checks for services

        return services.fold(mutableMapOf()) { acc, service ->
            acc[service.id] = try {
                collectForService(service)
            } catch (t: Throwable) {
                println(t.message)
                Pair(listOf(), listOf())
            }

            acc
        }
    }

    suspend fun sendDependencies(dir: String, data: MutableMap<Int, ManagerStatus>) {
        println("Sending dependencies to manager")

        val responses = client
            .sendProjectDependencies(data)
            .fold(mutableMapOf<Int, List<ActionResponse>>()) { acc, (manager, actions) ->
                try {
                    val service = manager.toService(dir)
                    acc[service.id] = executeActionsForService(service, actions)
                } catch (t: Throwable) {
                    println(t.message)
                }

                acc
            }

        client.sendActionResponses(responses)
    }

    private suspend fun collectForService(service: Service): ManagerStatus {
        println("Collecting dependency updates for ${service.type} in ${service.localPath}")

        return Pair(
            runChecks(service),
            service.manager.collectDependencies(service),
        )
    }

    private suspend fun runChecks(service: Service): List<Check> {
        val executor = GitLabActionExecutor(service) // TODO get proper executor

        return service.checks.fold(mutableListOf()) { acc, check ->
            acc.add(Check(check, executor.runCheck(check)))
            acc
        }
    }

    private suspend fun executeActionsForService(service: Service, actions: List<Action>): List<ActionResponse> {
        val executor = GitLabActionExecutor(service)
        println()

        if (actions.isEmpty()) {
            println("[${service.type}] Not executing any actions in ${service.localPath}")
            return listOf()
        }

        println("[${service.type}] Executing ${actions.size} actions in ${service.localPath}")
        executor.prepare()
        println()

        val responses = actions.mapIndexed { i, it ->
            println("[${service.type} ${i + 1}/${actions.size}] updating ${it.identifier}")
            executor.executeAction(it).also { println() }
        }

        println("[${service.type}] Cleaning up")
        executor.cleanUp()

        return responses
    }
}

suspend fun main(args: Array<String>) {

    val dir: String = File(if (args.isNotEmpty()) args[0] else ".").canonicalPath
    val collector = UpdateCollector()

    val services = collector.discoverServices(dir)
    val data = collector.collect(services)
    collector.sendDependencies(dir, data)
}
