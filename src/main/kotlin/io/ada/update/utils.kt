package io.ada.update

import io.ktor.http.*
import io.ktor.http.URLProtocol.Companion.HTTP
import io.ktor.http.URLProtocol.Companion.HTTPS
import java.io.File

fun String.execute(workDir: String) = this.execute(File(workDir))
fun String.execute(workDir: File) = exec(split(" "), workDir)

fun List<String>.execute(workDir: String) = this.execute(File(workDir))
fun List<String>.execute(workDir: File) = exec(this, workDir)

fun Process.readOutput() = this.inputStream
    .bufferedReader()
    .readText()
    .trim()

fun Process.readError() = this.errorStream
    .bufferedReader()
    .readText()
    .trim()

private fun exec(arguments: List<String>, workDir: File): Process = ProcessBuilder(*arguments.toTypedArray())
    .directory(workDir)
    .redirectOutput(ProcessBuilder.Redirect.PIPE)
    .redirectError(ProcessBuilder.Redirect.PIPE)
    .start()
    .also { it.waitFor() }

fun buildUrl(url: String, builder: URLBuilder) {

    val pattern = Regex("(?:(?<protocol>\\w*)://)?(?<host>[\\w.]+)(?::(?<port>\\d*)?)?(?<base>/.*)?")
    val result = pattern.find(url)?.groups ?: return

    builder.protocol = if (result["protocol"]?.value == "https") HTTPS else HTTP
    builder.host = result["host"]!!.value
    builder.port = result["port"]?.value?.toIntOrNull() ?: builder.protocol.defaultPort
}
