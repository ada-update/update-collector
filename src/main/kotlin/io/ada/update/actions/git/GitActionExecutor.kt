package io.ada.update.actions.git

import io.ada.update.actions.ActionExecutor
import io.ada.update.data.Service
import io.ada.update.portal.requests.ActionResponse
import io.ada.update.portal.responses.Action
import io.ada.update.readOutput

abstract class GitActionExecutor(service: Service) : ActionExecutor(service) {

    private var hasStash = false
    protected val baseBranch = "master"
    protected val remote = "origin"

    override fun prepare() {

        checkoutBaseBranch()

        println(" - stashing changes")
        hasStash = if (exec("git stash").readOutput() == "No local changes to save") {
            println(" - no changes to stash")
            false
        } else true
    }

    override suspend fun executeAction(action: Action): ActionResponse {

        val gaction = GitAction(action, service)
        val response = ActionResponse(action.identifier)

        checkoutBranchForAction(gaction.branch)
        println(" - updating using ${manager.name}")

        val success = try {
            manager.executeAction(service.path, action)
            true
        } catch (e: Throwable) {
            response.error = e.message
            false
        }

        if (success) {
            exec("git add .")

            println(" - committing changes")
            if (exec(listOf("git", "commit", "-m", getTitle(action))).exitValue() == 0) {
                println(" - pushing changes to $remote")
                exec("git push --set-upstream $remote ${gaction.branch}")
            } else {
                println(" - no changes detected")
            }

            response.merge_request = createMergeRequest(gaction)
        } else {
            println(" - could not update ${action.identifier}")
            exec("git reset --hard")
        }

        checkoutBaseBranch()
        println(" - delete ${gaction.branch}")
        exec("git branch -d ${gaction.branch}")

        return response
    }

    override fun cleanUp() {
        if (!hasStash) {
            return
        }

        println(" - restoring stashed changes")
        exec("git stash pop")
    }

    /** @return the url of the created merge request */
    protected abstract suspend fun createMergeRequest(action: GitAction): String

    private fun checkoutBaseBranch() {
        val ref = exec("git rev-parse --abbrev-ref HEAD").readOutput()

        if (ref != baseBranch) {
            println(" - checking out master")
            exec("git checkout $baseBranch")
        } else {
            println(" - already on master")
        }
    }

    private fun checkoutBranchForAction(branch: String) {
        when (exec("git ls-remote --exit-code --heads $remote $branch").exitValue()) {
            0 -> {
                println(" - branch $branch already exists on remote $remote")
                exec("git checkout -b $branch $remote/$branch")
            }
            2 -> {
                println(" - switching to branch $branch")
                exec("git branch $branch")
                exec("git checkout $branch")
            }
            else -> throw RuntimeException("Could not check if branch exists")
        }
    }
}
