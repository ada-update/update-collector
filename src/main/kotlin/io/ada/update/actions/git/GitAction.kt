package io.ada.update.actions.git

import io.ada.update.data.Service
import io.ada.update.portal.responses.Action

class GitAction(val action: Action, service: Service) {
    val identifier = action.identifier
    val spec = action.spec
    val fromVersion = action.fromVersion
    val toVersion = action.toVersion
    val branch = "deps/${managerName(service)}/${branchSafe(identifier)}"
}

private fun managerName(service: Service): String =
    service.manager.name + service.localPath.run { if (this == ".") "" else '-' + branchSafe(service.localPath) }

private fun branchSafe(input: String): String =
    input.replace('/', '-')
