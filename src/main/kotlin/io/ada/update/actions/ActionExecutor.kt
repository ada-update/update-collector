package io.ada.update.actions

import io.ada.update.data.Service
import io.ada.update.execute
import io.ada.update.portal.requests.ActionResponse
import io.ada.update.portal.responses.Action

abstract class ActionExecutor(val service: Service) {

    protected val manager = service.manager

    /**
     * Run a specific check.
     * @return true if the check passes, false if not
     */
    abstract suspend fun runCheck(check: String): Boolean

    abstract fun prepare()

    abstract suspend fun executeAction(action: Action): ActionResponse

    abstract fun cleanUp()

    protected fun getTitle(a: Action): String = "Update ${a.identifier} from ${a.fromVersion} to ${a.toVersion}"

    protected fun exec(cmd: String) = cmd.execute(service.path)

    protected fun exec(cmd: List<String>) = cmd.execute(service.path)
}
