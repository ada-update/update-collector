package io.ada.update.actions.gitlab

import io.ada.update.actions.gitlab.requests.CreateMergeRequestRequest
import io.ada.update.actions.gitlab.responses.MergeRequest
import io.ada.update.buildUrl
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class GitLabClient {

    private val projectId: Int = System.getenv("CI_PROJECT_ID")?.toIntOrNull() ?: 0
    private var gitlabUrl: String = System.getenv("CI_API_V4_URL") ?: "https://gitlab.com/api/v4"
    private val mergeRequestsUrl: String = "/api/v4/projects/$projectId/merge_requests"
    private val json = Json { ignoreUnknownKeys = true }

    private val token = System.getenv("CI_JOB_TOKEN") ?: System.getenv("PRIVATE_TOKEN")
    private val tokenHeader = if (System.getenv("PRIVATE_TOKEN") == null) "gitlab-ci-token" else "PRIVATE-TOKEN"

    private val httpClient = HttpClient(CIO) {
        defaultRequest {
            header(tokenHeader, token)
            accept(ContentType.Application.Json)
            url { buildUrl(gitlabUrl, this) }
        }
    }

    suspend fun getMergeRequest(id: Int): MergeRequest? {

        val response = httpClient.get("$mergeRequestsUrl/$id")

        return if (response.status == HttpStatusCode.OK) {
            json.decodeFromString(response.bodyAsText())
        } else null

    }

    suspend fun getMergeRequestsForBranch(branch: String, open: Boolean = true): List<MergeRequest> {

        val response = httpClient.get(mergeRequestsUrl) {
            if (open) parameter("state", "opened")
            parameter("source_branch", branch)
        }

        return json.decodeFromString(response.bodyAsText())
    }

    suspend fun createMergeRequest(title: String, sourceBranch: String, targetBranch: String): MergeRequest {

        val response = httpClient.post(mergeRequestsUrl) {
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(CreateMergeRequestRequest(sourceBranch, targetBranch, title)))
        }

        return json.decodeFromString(response.bodyAsText())
    }
}
