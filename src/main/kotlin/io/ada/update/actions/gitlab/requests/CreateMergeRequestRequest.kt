package io.ada.update.actions.gitlab.requests

import kotlinx.serialization.Serializable

@Serializable
data class CreateMergeRequestRequest(
    val source_branch: String,
    val target_branch: String,
    val title: String,
)