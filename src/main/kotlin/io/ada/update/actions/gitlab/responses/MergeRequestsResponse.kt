package io.ada.update.actions.gitlab.responses

import kotlinx.serialization.Serializable

@Serializable
data class MergeRequest(
    val web_url: String,
    val state: String,
)
