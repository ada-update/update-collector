package io.ada.update.actions.gitlab

import io.ada.update.actions.git.GitAction
import io.ada.update.actions.git.GitActionExecutor
import io.ada.update.data.Service

class GitLabActionExecutor(service: Service) : GitActionExecutor(service) {

    private val client = GitLabClient()

    override suspend fun createMergeRequest(action: GitAction): String {

        val mrs = client.getMergeRequestsForBranch(action.branch)

        if (mrs.isNotEmpty()) {
            println(" - merge request already exists for ${action.branch}")


            // TODO update title

            return mrs[0].web_url
        }

        return client.createMergeRequest(getTitle(action.action), action.branch, baseBranch).web_url
    }

    override suspend fun runCheck(check: String): Boolean {

        val mergeRequest = client.getMergeRequest(getIdFromCheck(check)) ?: return false

        return mergeRequest.state == "opened"
    }

    private fun getIdFromCheck(check: String): Int {
        val regex = Regex(".*/(?<id>\\d+)")
        val match = regex.matchEntire(check)!!.groups

        return match["id"]!!.value.toInt()
    }
}
