package io.ada.update.portal.requests

import kotlinx.serialization.Serializable

@Serializable
data class ProjectPatchActionsRequest(
    val actions: List<ProjectActions>,
)

@Serializable
data class ProjectActions(
    val manager_id: Int,
    val actions: List<ActionResponse>,
)

@Serializable
data class ActionResponse(
    val identifier: String,
    var merge_request: String? = null,
    var error: String? = null,
)
