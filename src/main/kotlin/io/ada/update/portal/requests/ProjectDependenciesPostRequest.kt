package io.ada.update.portal.requests

import io.ada.update.data.Check
import io.ada.update.data.Dependency
import kotlinx.serialization.Serializable

@Serializable
data class ProjectDependenciesPostRequest(
    val dependencies: List<ProjectData>,
)

@Serializable
data class ProjectData(
    val manager_id: Int,
    val checks: List<Check>,
    val updates: List<Dependency>,
)
