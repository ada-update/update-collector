package io.ada.update.portal.requests

import io.ada.update.data.ManagerData
import kotlinx.serialization.Serializable

@Serializable
data class ProjectConfigPatchRequest(
    val managers: List<ManagerData>,
)
