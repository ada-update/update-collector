package io.ada.update.portal

import io.ada.update.buildUrl
import io.ada.update.data.Check
import io.ada.update.data.Dependency
import io.ada.update.data.ManagerData
import io.ada.update.portal.data.ProjectManager
import io.ada.update.portal.requests.*
import io.ada.update.portal.responses.ActionedService
import io.ada.update.portal.responses.ProjectActionsResponse
import io.ada.update.portal.responses.ProjectConfigResponse
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class PortalClient {

    private val projectSecret = System.getenv("ADA_UPDATE_SECRET")
    private val baseUrl = System.getenv("ADA_UPDATE_ENDPOINT") ?: "https://mgr.adaupdate.io"

    private val httpClient = HttpClient(CIO) {
        defaultRequest {
            accept(ContentType.Application.Json)
            url { buildUrl(baseUrl, this) }
        }
    }

    /**
     * Sends a list of discovered services to the manager.
     * The manager will return a list of services to handle.
     */
    suspend fun patchManagers(managers: List<ManagerData>): List<ProjectManager> {

        val response = httpClient.patch("/api/v1/projects/$projectSecret/managers") {
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(ProjectConfigPatchRequest(managers)))
        }

        checkResponse(response, HttpStatusCode.OK)

        return Json.decodeFromString<ProjectConfigResponse>(response.bodyAsText()).data
    }

    suspend fun sendProjectDependencies(data: Map<Int, Pair<List<Check>, List<Dependency>>>): List<ActionedService> {

        val dependencies = data.map { (id, pair) -> ProjectData(id, pair.first, pair.second) }
        val response = httpClient.patch("/api/v1/projects/$projectSecret/dependencies") {
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(ProjectDependenciesPostRequest(dependencies)))
        }

        checkResponse(response, HttpStatusCode.OK)

        return Json.decodeFromString<ProjectActionsResponse>(response.bodyAsText()).data
    }

    suspend fun sendActionResponses(data: Map<Int, List<ActionResponse>>) {

        val actions = data.map { (id, actions) -> ProjectActions(id, actions) }
        val response = httpClient.patch("/api/v1/projects/$projectSecret/actions") {
            contentType(ContentType.Application.Json)
            setBody(Json.encodeToString(ProjectPatchActionsRequest(actions)))
        }

        checkResponse(response, HttpStatusCode.Accepted)
    }

    private suspend fun checkResponse(response: HttpResponse, expectedStatus: HttpStatusCode) {
        if (response.status != expectedStatus) {
            throw IllegalStateException("Received invalid status: ${response.status}\n\n${response.bodyAsText()}")
        }
    }
}
