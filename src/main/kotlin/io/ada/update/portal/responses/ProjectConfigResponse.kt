package io.ada.update.portal.responses

import io.ada.update.portal.data.ProjectManager
import kotlinx.serialization.Serializable

@Serializable
data class ProjectConfigResponse(
    val data: List<ProjectManager>
)
