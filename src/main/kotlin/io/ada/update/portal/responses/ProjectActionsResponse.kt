package io.ada.update.portal.responses

import io.ada.update.portal.data.ProjectManager
import kotlinx.serialization.Serializable

@Serializable
data class ProjectActionsResponse(
    val data: List<ActionedService>,
)

@Serializable
data class ActionedService(
    val manager: ProjectManager,
    val actions: List<Action>,
)

@Serializable
data class Action(
    val identifier: String,
    val spec: String,
    val fromVersion: String,
    val toVersion: String,
)
