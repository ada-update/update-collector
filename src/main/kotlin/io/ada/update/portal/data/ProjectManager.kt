package io.ada.update.portal.data

import io.ada.update.data.Service
import kotlinx.serialization.Serializable
import java.io.File

@Serializable
data class ProjectManager(
    val id: Int,
    val type: String,
    val path: String,
    val checks: List<String>,
) {
    fun toService(dir: String): Service {
        val path = File(dir + "/" + this.path)

        if (!path.exists()) {
            throw IllegalStateException("'$path' does not exist")
        }

        return Service(this.id, this.type, path.path, this.path, this.checks)
    }
}
