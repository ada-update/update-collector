package io.ada.update.managers.composer

import io.ada.update.readError

class ComposerUpdateException : RuntimeException {

    constructor(process: Process) : super(findErrors(process.readError()))

    constructor(message: String) : super(message)
}

private fun findErrors(input: String): String = input
    .substringAfter("Your requirements could not be resolved to an installable set of packages.")
    .substringBefore("Installation failed, reverting ./composer.json and ./composer.lock to their original content.")
    .trim()
    .split("\n")
    .joinToString("\n") { it.trim() }
