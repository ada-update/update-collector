package io.ada.update.managers

import io.ada.update.data.Dependency
import io.ada.update.data.Service
import io.ada.update.portal.responses.Action
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import kotlin.io.path.relativeTo

abstract class PackageManager {

    abstract val name: String

    abstract val ignoredFolders: List<String>

    fun discoverManagers(directory: String): List<String> {
        val path = Path.of(directory)

        return Files
            .find(path, 10, { current, attributes ->
                attributes.isRegularFile && isServiceFile(current)
            })
            .collect(Collectors.toList())
            .map { it.relativeTo(path) }
            .map { it.parent ?: "." }
            .map { it.toString() }
    }

    protected abstract fun isServiceFile(path: Path): Boolean

    abstract fun collectDependencies(service: Service): List<Dependency>

    /**
     * @throws any error that comes up during execution
     * @return true if the action was executed successfully
     */
    abstract fun executeAction(path: String, action: Action)
}
