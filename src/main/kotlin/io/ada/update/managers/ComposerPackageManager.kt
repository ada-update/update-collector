package io.ada.update.managers

import io.ada.update.data.Dependency
import io.ada.update.data.Service
import io.ada.update.execute
import io.ada.update.managers.composer.ComposerUpdateException
import io.ada.update.portal.responses.Action
import io.ada.update.readOutput
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.Path

class ComposerPackageManager : PackageManager() {

    private val json = Json { ignoreUnknownKeys = true }
    override val name = "composer"
    override val ignoredFolders = listOf("vendor")

    override fun isServiceFile(path: Path): Boolean {
        return !path.toString().split("/").contains("vendor")
            && path.fileName.toString() == "composer.lock"
    }

    override fun collectDependencies(service: Service): List<Dependency> {

        if (!File(service.path + "/composer.lock").exists()) {
            println("missing composer.lock in \"${service.path}\"")
            return listOf()
        }

        composerInstall(service.path)

        val dependencies = getComposerConfig(service.path)
            .run { require.toMutableMap().also { it.putAll(`require-dev`) } }

        val versions = "composer show --format=json -D -l"
            .execute(service.path)
            .run { Json.decodeFromString<ComposerData>(readOutput()) }
            .installed

        return versions.map {
            Dependency(
                identifier = it.name,
                version_spec = dependencies[it.name]!!,
                current_version = it.version,
                newest_version = it.latest,
            )
        }
    }

    override fun executeAction(path: String, action: Action) {
        val spec = if (action.spec.startsWith("^")) "^" else ""

        composerInstall(path)

        val composer = getComposerConfig(path)

        val cmd = if (composer.require.containsKey(action.identifier)) {
            "require"
        } else if (composer.`require-dev`.containsKey(action.identifier)) {
            "require --dev"
        } else {
            throw ComposerUpdateException("Package not found in composer.json")
        }

        println(" - running $cmd")
        val process = "composer $cmd --ignore-platform-reqs ${action.identifier}:${spec}${action.toVersion} -W"
            .execute(path)

        if (process.exitValue() != 0) {
            throw ComposerUpdateException(process)
        }
    }

    private fun composerInstall(path: String) {
        "composer install --ignore-platform-reqs".execute(path)
    }

    private val composerCache = mutableMapOf<String, ComposerPackage>()

    private fun getComposerConfig(path: String): ComposerPackage {
        if (composerCache.containsKey(path)) {
            return composerCache[path]!!
        }

        val composer = json.decodeFromString<ComposerPackage>(File("$path/composer.json").readText())

        composerCache[path] = composer

        return composer
    }
}

@Serializable
private data class ComposerPackage(
    val require: Map<String, String> = mapOf(),
    val `require-dev`: Map<String, String> = mapOf(),
)

@Serializable
private data class ComposerData(
    val installed: List<ComposerDependency>
)

@Serializable
private data class ComposerDependency(
    val name: String,
    val version: String,
    val latest: String,
    val `latest-status`: String,
    val description: String,
)
