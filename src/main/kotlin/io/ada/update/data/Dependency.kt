package io.ada.update.data

import kotlinx.serialization.Serializable

@Serializable
data class Dependency(
    val identifier: String,
    val version_spec: String,
    val current_version: String,
    val newest_version: String,
)
