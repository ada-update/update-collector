package io.ada.update.data

import io.ada.update.managers.PackageManager
import io.ada.update.packageManagerRegistry
import kotlinx.serialization.Serializable

@Serializable
class Service(
    val id: Int,
    val type: String,
    val path: String,
    val localPath: String,
    val checks: List<String>,
) {
    val manager: PackageManager
        get() = packageManagerRegistry[type] ?: throw IllegalStateException("'$type' is not supported")
}
