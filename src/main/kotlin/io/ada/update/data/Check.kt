package io.ada.update.data

import kotlinx.serialization.Serializable

@Serializable
data class Check(
    val name: String,
    val value: Boolean,
)
