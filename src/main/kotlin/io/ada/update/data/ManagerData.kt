package io.ada.update.data

import kotlinx.serialization.Serializable

@Serializable
data class ManagerData(
    val type: String,
    val path: String,
)
