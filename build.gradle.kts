import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm") version "1.6.20"
    kotlin("plugin.serialization") version "1.6.20"
}

group = "io.ada"
val version: String by project

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
    implementation("io.ktor:ktor-client-core:2.0.1")
    implementation("io.ktor:ktor-client-cio:2.0.1")
    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.2")
}

application {
    mainClass.set("io.ada.update.UpdateCollectorKt")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks {
    val standalone = register<Jar>("standalone") {
        dependsOn("processResources", "compileKotlin", "compileJava")
        manifest { attributes(mapOf("Main-Class" to application.mainClass)) }

        archiveClassifier.set("standalone")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE

        val mainSources = sourceSets.main.get().output
        val runtimeDeps = configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }

        from(mainSources + runtimeDeps)
    }

    assemble { dependsOn(standalone) }
    build { dependsOn(standalone) }
}
